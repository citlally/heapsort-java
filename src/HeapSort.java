public class HeapSort { 
	
	public static void main(String args[]) { 
		int a[] = {12, 11, 13, 5, 29, 6, 7, 53};

		System.out.println("Array original:");
		imprimirArray(a);
		
		//Implementamos el HeapSort (ordenamiento por monticulo)
		HeapSort h = new HeapSort(); 
		h.heapSort(a); 
		
		System.out.println("\n\nOrdenado:"); 
		imprimirArray(a); 
	} 
	
	static void imprimirArray(int arr[]) {
		for (int i=0; i < arr.length; ++i) 
			System.out.print(arr[i]+" ");
	} 
	
// -------------- ORDENAMIENTO DE MONTÍCULO ----------------------
	
	public void heapSort(int[] arr) {
		
		buildMaxHeap(arr);
		
		//---- Toma el montículo y lo pone al final de la lista / detras del más grande.
		for (int i=arr.length; i >= 2; i--) {
			int temp = arr[0]; 
			arr[0] = arr[i-1]; 
			arr[i-1] = temp; 

			//El tamaño  debe reducirse en 1 por que no se toma en cuenta los monticulos que ya se quitaron.
			maxHeapify(arr, i-1, 1);
		}
		
	}
	
	public void buildMaxHeap(int arr[]) { 

		for (int i = arr.length / 2; i >= 1; i--) {
			maxHeapify(arr, arr.length, i); 
		}

	} 

	// Acomodar una parte del arbol binario cuyo padre es i
	void maxHeapify(int arr[], int n, int i) { 
		int largest = i; // Parent
		int l = left(i); // left 
		int r = right(i); // right 
		//System.out.println(largest + " " + l + " " + r);

		// izquierdo (left) más grande
		if (l <= n && arr[l-1] > arr[largest-1]) {
			largest = l;
		} 

		// derecho (right) es el más grande 
		if (r <= n && arr[r-1] > arr[largest-1]) {
			largest = r; 
		}

		// Si el más grande no es el padre, entonces intercambiamos
		if (largest != i) { 
			int temp = arr[i-1]; 
			arr[i-1] = arr[largest-1]; 
			arr[largest-1] = temp; 

			// volvemos a llamar al método:
			maxHeapify(arr, n, largest); 
		} 
	}
	
	public int left(int i) {
		return 2 * i;
	}
	
	public int right(int i) {
		return 2 * i + 1;
	}

} 


